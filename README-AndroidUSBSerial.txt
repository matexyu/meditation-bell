---------------------------------------------------------
 Android device to Arduino: USB OTG Serial Communication
---------------------------------------------------------

The Arduino device is connected to the Android device via an USB OTG cable:

The "USB Serial for Android" library is used to communicate with the Arduino device.

  https://github.com/mik3y/usb-serial-for-android/releases


A recent precompiled JAR for "USB Serial for Android" can be found inside of the processing-android-serial library ZIP file:

  https://github.com/inventit/processing-android-serial/releases

The precompiled JAR has been already put inside the Sketch 'code' sub-folder (as of January 2017).

The AndroidManifest.xml file has already been edited to allow USB OTG communication,
and to automatically launch the application when the Arduino is plugged into the Android device.



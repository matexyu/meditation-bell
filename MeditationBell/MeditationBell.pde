
import android.content.Context;

import android.content.SharedPreferences;

import android.app.TimePickerDialog;
import android.widget.TimePicker;

import android.os.Vibrator;
  
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDeviceConnection;

import java.util.List;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

//----------------------------------------------------
long timerDurationSeconds = 20 * 60;
int numberOfBellPings = 3;
int intervalBetweenPingsMs = 3000;
int vibrationDurationMs = 1000;
//----------------------------------------------------

long previousTimeSeconds = 0;

boolean wasTimerToggleRequested = false;
boolean isTimerRunning = false;
boolean isEndingBellRequested = false;
long initialTimeMs;

float flashValue;
float screenDiagonalPx;
float timeCircleAlpha = 0;
float targetTimeCircleAlpha;

PFont font;
int textSize = 128;

Vibrator vibratorService;

UsbSerialPort port;

PVector screenCenter = new PVector();

void showDurationDialog()
{
  getActivity().runOnUiThread(new Runnable() 
  {
     public void run() 
     {
       showDurationDialogImpl();
     }
  });
}

void showDurationDialogImpl()
{
  final int hours = (int)(timerDurationSeconds / 60) / 60;
  final int minutes = (int)(timerDurationSeconds / 60) % 60;
  
  TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), 
  new TimePickerDialog.OnTimeSetListener() 
  {
      public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) 
      {
        timerDurationSeconds = (selectedHour * 60 + selectedMinute) * 60;
        println("Time set (hh:mm) = " + selectedHour + ":" + selectedMinute + 
                ", overall minutes = " + (timerDurationSeconds / 60.0));
        
        println("Saving timer duration in preferences...");
        {
          SharedPreferences sp = getActivity().getPreferences(Context.MODE_PRIVATE);
          SharedPreferences.Editor editor = sp.edit();
          editor.putInt("timerDurationSeconds", (int)timerDurationSeconds);
          editor.commit();
        }
      }
  }, 
  hours, minutes, 
  true // 24 hours time
  ); 
  
  timePickerDialog.setTitle("Select Duration (hh:mm)");
  timePickerDialog.setCancelable(true);
  timePickerDialog.setCanceledOnTouchOutside(true);
  
  timePickerDialog.show();
}

void setup()
{
  fullScreen();
  
  background(0);
  
  println("Screen resolution (W x H): " + width + " x " + height);
  
  textSize = max(height, width) / 15;
  
  screenDiagonalPx = sqrt(width * width + height * height);
  
  String charset = "1234567890:";
  font = createFont("roboto-regular.ttf", textSize, true, charset.toCharArray());
  textFont(font);
  
  ellipseMode(CENTER);
  
  initSerialPort();
  
  if (port != null)
  {
    println("Opened Serial Port Device: " + port.toString());
  }
  else
  {
    println("Unable to open serial port!");
  }
  
  vibratorService = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
  
  println("Keeping screen on...");
  getActivity().runOnUiThread(new Runnable() 
  {
     public void run()
     {
       getView().setKeepScreenOn(true);
     }
  });
  
  targetTimeCircleAlpha = 0.25;
  
  SharedPreferences sp = getActivity().getPreferences(Context.MODE_PRIVATE);
  timerDurationSeconds = sp.getInt("timerDurationSeconds", (int)timerDurationSeconds);
  println("Loaded timer duration from preferences: " + timerDurationSeconds + " seconds");
}

int getTimeCircleSize()
{
  int padding = 64;
  return min(width, height) - padding * 2;
}

void draw()
{
  final float frameDeltaTimeSeconds = (frameRate <= 0.1 ? 0.0 : 1.0 / frameRate);
  
  // recompute each time (in case of a change in screen size / orientation)
  screenCenter.set(width / 2, height / 2);
  
  if (wasTimerToggleRequested)
  {
    if (isTimerRunning == false)
    {
      if (port != null)
      {
        thread("pingBellRepeatedly");
      }
      else if (vibratorService.hasVibrator())
      {
        vibratorService.vibrate(vibrationDurationMs);
      }
      
      initialTimeMs = millis();
      
      isTimerRunning = true;
      isEndingBellRequested = false;
      
      targetTimeCircleAlpha = 1.0;
    }
    else
    {
      isTimerRunning = false;
      isEndingBellRequested = false;
      
      targetTimeCircleAlpha = 0.25;
    }
    
    wasTimerToggleRequested = false;
  }
  
  // Animate the "time circle" alpha
  {
    float deltaAlpha = frameDeltaTimeSeconds;
    
    if (timeCircleAlpha != targetTimeCircleAlpha)
    {
      if (timeCircleAlpha < targetTimeCircleAlpha)
      {
        timeCircleAlpha += deltaAlpha;
        timeCircleAlpha = min(timeCircleAlpha, targetTimeCircleAlpha);
      }
      else
      {
        timeCircleAlpha -= deltaAlpha;
        timeCircleAlpha = max(timeCircleAlpha, targetTimeCircleAlpha);
      }
    }
  }
  
  final long elapsedTimeSeconds = (isTimerRunning ? (millis() - initialTimeMs) / 1000 : 0);
  final long remainingTimeSeconds = timerDurationSeconds - elapsedTimeSeconds;
  
  if (remainingTimeSeconds < 0 && 
      isEndingBellRequested == false)
  {
    if (port != null)
    {
      thread("pingBellRepeatedly");
    }
    else if (vibratorService.hasVibrator())
    {
      vibratorService.vibrate(vibrationDurationMs);
    }
    
    isEndingBellRequested = true;
  }
  
  background(0);
  
  // Draw the "time circle"
  {
    noFill();
    
    strokeWeight(15);
    int arc_size = getTimeCircleSize();
    
    stroke(255, 255, 255, timeCircleAlpha * 127.0);
    ellipse(screenCenter.x, screenCenter.y, 
            arc_size, arc_size);
    
    if (isTimerRunning)
    {
      float completion = elapsedTimeSeconds / (float)timerDurationSeconds;
      
      stroke(255, 32, 32, 128);
      arc(screenCenter.x, screenCenter.y, 
          arc_size, arc_size, 
          0, completion * 2.0 * PI, 
          OPEN);
    }
  }
  
  if (flashValue > 0.0)
  {
    noStroke();
    float k = flashValue;
    float r = screenDiagonalPx *1.5 * sqrt(1.0 - k);
    fill(255, 255, 255, pow(k, 4.0) * 255.0);
    ellipse(screenCenter.x, screenCenter.y, r, r);
    float decayRate = 1.0 / (intervalBetweenPingsMs / 1000.0);
    
    flashValue -= frameDeltaTimeSeconds * decayRate;
  }
  
  // Render Remaining Time text:
  {
    long displayTimeSeconds = (int)max(remainingTimeSeconds, 0);
    long hh = (displayTimeSeconds / 60) / 60;
    long mm = (displayTimeSeconds / 60) % 60;
    long ss = displayTimeSeconds % 60;
    
    noStroke();
    fill(255);
    
    textAlign(CENTER);
    textSize(textSize);
    text(String.format("%02d", hh) + ":" + 
         String.format("%02d", mm) + ":" + 
         String.format("%02d", ss), 
         screenCenter.x, screenCenter.y);
  }
  
}

void mousePressed()
{
  PVector center_to_mouse = new PVector(mouseX, mouseY);
  center_to_mouse.sub(screenCenter);
  
  final boolean isPortraitMode = (height >= width);
  final int timeCircleRadius = getTimeCircleSize() / 2;
  
  // 1. Text area clicked: start timer
  if (abs(mouseY - screenCenter.y) <= textSize &&
      abs(mouseX - screenCenter.x) <= timeCircleRadius)
  {
    wasTimerToggleRequested = true;
  }
  // 2. Area inside the "time circle" touched, open duration settings dialog
  else if (center_to_mouse.mag() <= timeCircleRadius && 
           isTimerRunning == false)
  {
    println("Open duration settings dialog...");
    showDurationDialog();
  }
  // 3. Top (portrait) or right half (landscape): test bell hammer actuator 
  else if ((isPortraitMode  && mouseY < height / 5       ) ||
           (!isPortraitMode && mouseX > width - width / 5))
  {
    flashValue = 1.0;
    pingBell();
  }
}

boolean isPingingBell = false;

void pingBellRepeatedly()
{
  if (isPingingBell == false)
  {
    isPingingBell = true;
    
    for (int i = 0; i < numberOfBellPings; i++)
    {
      println("PING " + i);
      pingBell();
      flashValue = 1.0;
      delay(intervalBetweenPingsMs);
    }
    
    isPingingBell = false;
  }
}

byte serialBuffer[] = new byte[1];

void pingBell()
{
  if (port != null)
  {
    serialBuffer[0] = 13;
    
    try
    {
      int timeoutMillis = 500;
      port.write(serialBuffer, timeoutMillis);
    }
    catch (IOException e) 
    {
      println("IOException while closing serial port: " + e.toString());
    } 
  }
  else if (vibratorService.hasVibrator())
  {
    vibratorService.vibrate(vibrationDurationMs);
  }
}

void stop() // Called at sketch termination 
{
  if (port != null)
  {
    try 
    {
      port.close();
    }
    catch (IOException e) 
    {
      println("IOException while closing serial port: " + e.toString());
    } 
  }
  
}

//==================================================================

void initSerialPort()
{
  // Find all available drivers from attached devices.
  UsbManager manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
  List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
  
  if (availableDrivers.isEmpty()) 
  {
    println("No USB Drivers were found.");
    return;
  }
  
  // Open a connection to the first available driver.
  UsbSerialDriver driver = availableDrivers.get(0);
  UsbDeviceConnection connection = manager.openDevice(driver.getDevice());
  
  if (connection == null) 
  {
    println("You probably need to call UsbManager.requestPermission(driver.getDevice(), ...)");
    return;
  }
  
  // Most have just one port (port 0).
  UsbSerialPort new_port = driver.getPorts().get(0);
  
  if (new_port != null)
  {
    try 
    {
      println("Opening new serial port...");
      new_port.open(connection);
      new_port.setParameters(9600, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
      
      if (port != null)
      {
        println("Closing previously open port, " + port.toString());
        port.close();
      }
      
      port = new_port;
    } 
    catch (IOException e) 
    {
      // Deal with error.
      println("Error while opening Serial port: " + e.toString());
    } 
  }
}
 
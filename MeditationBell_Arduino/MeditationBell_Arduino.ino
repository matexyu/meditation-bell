
/*

   Whatever-serial-byte to digital output pulse:

   Mathieu Bosi 2017

*/

unsigned long triggerTimeMs = 0;

bool          isOutputPinActive = false;
const int     impulseDurationMs = 50;

const int     hBridgePinA1 = 11;
const int     hBridgePinA2 = 12;

bool isHBridgeEnabled = false;

void setup() 
{
  Serial.begin(9600);
  setHBridgeEnabled(false);
}

void setHBridgeEnabled(bool do_enable)
{
  pinMode(hBridgePinA1, do_enable ? OUTPUT : INPUT);
  pinMode(hBridgePinA2, do_enable ? OUTPUT : INPUT);
  isHBridgeEnabled = do_enable;
}

void setHBridgeState(bool a1, bool a2)
{
  if (isHBridgeEnabled)
  {
    digitalWrite(hBridgePinA1, a1);
    digitalWrite(hBridgePinA2, a2);
  }
}

void attract()
{
  setHBridgeState(0, 1);
}

void repel()
{
  setHBridgeState(1, 0);
}

void loop() 
{
  if (Serial.available() > 0)
  {
    byte received = Serial.read();
    triggerTimeMs = millis();
    
    setHBridgeEnabled(true);
    attract();
    
    isOutputPinActive = true;
  }

  if (isOutputPinActive)
  {
    long elapsed_time = millis() - triggerTimeMs;
    if (elapsed_time > impulseDurationMs)
    {
      repel();
      delay(impulseDurationMs / 2);
      setHBridgeEnabled(false);
    
      isOutputPinActive = false;
    }
  }
  
}
